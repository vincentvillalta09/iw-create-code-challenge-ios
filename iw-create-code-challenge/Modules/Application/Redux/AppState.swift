//
//  AppState.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift

struct AppState: StateType {
    var homeState: HomeState = HomeState.makeDefault()
}
