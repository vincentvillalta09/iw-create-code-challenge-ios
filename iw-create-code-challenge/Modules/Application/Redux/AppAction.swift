//
//  AppAction.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift

enum AppAction { /* Empty enum for namespacing */ }

protocol AppActionable: Action {
    func reduce(state: inout AppState)
}
