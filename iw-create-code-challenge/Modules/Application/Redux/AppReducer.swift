//
//  AppReducer.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift

func appReducer(action: Action, state: AppState?) -> AppState {
    var state = AppState(
        homeState: homeReducer(state: state?.homeState, action: action)
    )
    
    switch action {
    case let action as AppActionable:
        action.reduce(state: &state)
    default:
        break
    }
    
    return state
}
