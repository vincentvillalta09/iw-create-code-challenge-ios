//
//  PokemonDetailsViewController.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit
import ReSwift

class PokemonDetailsViewController: UIViewController {
    private let store: Store<AppState>
    private var state: HomeState?
    
    private lazy var detailView: PokemonDetailsView = {
        let view = PokemonDetailsView()
        return view
    }()
    
    init(store: Store<AppState> = Dependency.shared.store) {
        self.store = store
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    override func loadView() {
        self.view = detailView
    }
}

extension PokemonDetailsViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = HomeState

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.subscribe(self) { state in
            return state.select({$0.homeState}).skipRepeats()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
        store.dispatch(HomeAction.selectPokemon(nil))
    }
    
    func newState(state: StoreSubscriberStateType) {
        self.state = state
        let viewModel = HomeViewModel(state)
        guard let pokemon = viewModel.selectedPokemon else { return }
        detailView.configureWith(pokemon)
    }
}
