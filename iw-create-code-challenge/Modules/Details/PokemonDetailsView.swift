//
//  PokemonDetailsView.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

class PokemonDetailsView: View {
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .white
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.textColor = .white
        return label
    }()
    
    override func commonInit() {
        super.commonInit()
        
        backgroundColor = UIColor.App.darkPurple
        
        addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(180.0)
        }
        addSubview(nameLabel)
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(8.0)
            make.top.equalTo(imageView.snp.bottom).offset(10)
        }
    }
    
    func configureWith(_ pokemon: Pokemon) {
        if let spriteURL = pokemon.sprites?["front_default"] {
            imageView.kf.setImage(with: URL(string: spriteURL))
        }
        self.nameLabel.text = pokemon.name.capitalized
        
        if let abilitiesView = createListOfAbilities(pokemon.abilities) {
            addSubview(abilitiesView)
            abilitiesView.snp.makeConstraints { make in
                make.leading.trailing.equalToSuperview().inset(8.0)
                make.top.equalTo(self.nameLabel.snp.bottom).offset(18)
            }
        }
    }
    
    private func createListOfAbilities(_ abilities: [Abilities]?) -> UIView? {
        guard let abilities = abilities else { return nil }
        let stackView = UIStackView()
        stackView.distribution = .fillProportionally
        stackView.axis = .horizontal
        
        let title = UILabel()
        title.textColor = UIColor.App.pink
        title.text = "Habilidades"
        title.font = UIFont.boldSystemFont(ofSize: 17)
        
        let wrapper = UIView()
        wrapper.backgroundColor = .clear
        wrapper.addSubview(title)
        title.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
        }
        //return wrapper
        for ability in abilities.sorted(by: {$0.slot < $1.slot}) {
            let abilityLabel = UILabel()
            abilityLabel.textColor = UIColor.white
            abilityLabel.text = ability.ability.name
            abilityLabel.font = UIFont.systemFont(ofSize: 14)
            abilityLabel.layer.cornerRadius = 4
            abilityLabel.layer.borderWidth = 1
            abilityLabel.layer.borderColor = UIColor.App.pink.cgColor
            stackView.addArrangedSubview(abilityLabel)
        }
        
        wrapper.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.leading.bottom.equalToSuperview()
            make.top.equalTo(title.snp.bottom).offset(10)
        }
        
        return wrapper
    }
}
