//
//  HomeState.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift

struct HomeState: StateType, Equatable {
    var isLoading: Bool
    var pokemons: [Pokemon]
    var selection: Pokemon?
}

extension HomeState {
    static func makeDefault() -> HomeState {
        return HomeState(
            isLoading: true,
            pokemons: [],
            selection: nil
        )
    }
}

