//
//  HomeReducer.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift

func homeReducer(state: HomeState?, action: Action) -> HomeState {
    var state = state ?? HomeState.makeDefault()
    
    switch action {
    case let action as HomeActionable:
        action.reduce(state: &state)
    default:
        break
    }
    
    return state
}
