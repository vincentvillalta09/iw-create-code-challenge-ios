//
//  HomeAction.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift
import PromiseKit
import ReSwiftThunk

enum HomeAction { /* Empty enum for namespacing */ }

protocol HomeActionable: Action {
    func reduce(state: inout HomeState)
}

extension HomeAction {
    struct Reset: HomeActionable {
        func reduce(state: inout HomeState) {
            state = .makeDefault()
        }
    }
    
    static func getPokemonList() -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            dispatch(getPokemonListTrigger())
            firstly {
                HomeService().requestPokemonList(offset: getState()?.homeState.pokemons.count ?? 0)
            }.done { response in
                dispatch(getPokemonListSuccess(response))
                for pokemon in response.results {
                    dispatch(getDetailsForPokemon(pokemon))
                }
            }.catch { error in
                dispatch(getPokemonListFailure(error))
            }
        }
    }
    
    struct getPokemonListTrigger: HomeActionable {
        func reduce(state: inout HomeState) {
            state.isLoading = true
            
        }
    }
    
    struct getPokemonListSuccess: HomeActionable {
        let apiResponse: PokemonAPIResponse
        init(_ response: PokemonAPIResponse) {
            self.apiResponse = response
        }
        
        func reduce(state: inout HomeState) {
            state.isLoading = false
            state.pokemons.append(contentsOf: apiResponse.results)
        }
    }
    
    struct getPokemonListFailure: HomeActionable {
        let apiError: Error
        init(_ error: Error) {
            self.apiError = error
        }
        
        func reduce(state: inout HomeState) {
            state.isLoading = false
        }
    }
    
    static func getDetailsForPokemon(_ pokemon: Pokemon) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard pokemon.abilities == nil else { return }
            firstly {
                HomeService().requestPokemonDetails(pokemon)
            }.done { result in
                dispatch(getPokemonDetailsSuccess(pokemon, apiResponse: result))
            }.catch { error in
                print(error)
            }
        }
    }
    
    struct getPokemonDetailsSuccess: HomeActionable {
        let response: PokemonDetailsAPIResponse
        let pokemon: Pokemon
        init(_ pokemon: Pokemon, apiResponse: PokemonDetailsAPIResponse) {
            self.response = apiResponse
            self.pokemon = pokemon
        }
        
        func reduce(state: inout HomeState) {
            var updatedPokemon = pokemon
            updatedPokemon.abilities = response.abilities
            updatedPokemon.forms = response.forms
            updatedPokemon.height = response.height
            if let index = state.pokemons.firstIndex(where: {$0.name == pokemon.name}) {
                state.pokemons.remove(at: index)
                state.pokemons.insert(updatedPokemon, at: index)
                Dependency.shared.store.dispatch(getFormsForPokemon(updatedPokemon))
            }
            
        }
    }
    
    static func getFormsForPokemon(_ pokemon: Pokemon) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            guard let form = pokemon.forms?.first else { return }
            firstly {
                HomeService().requestPokemonImages(form)
            }.done { result in
                dispatch(getPokemonFormsSuccess(pokemon, apiResponse: result))
            }.catch { error in
                print(error)
            }
        }
    }
    
    struct getPokemonFormsSuccess: HomeActionable {
        let response: PokemonFormAPIResponse
        let pokemon: Pokemon
        init(_ pokemon: Pokemon, apiResponse: PokemonFormAPIResponse) {
            self.response = apiResponse
            self.pokemon = pokemon
        }
        
        func reduce(state: inout HomeState) {
            var updatedPokemon = pokemon
            updatedPokemon.sprites = response.sprites
            if let index = state.pokemons.firstIndex(where: {$0.name == pokemon.name}) {
                state.pokemons.remove(at: index)
                state.pokemons.insert(updatedPokemon, at: index)
            }
        }
    }
    
    static func selectPokemon(_ pokemon: Pokemon?) -> Thunk<AppState> {
        return Thunk<AppState> { dispatch, getState in
            dispatch(reducePokemonSelection(pokemon))
        }
    }
    
    struct reducePokemonSelection: HomeActionable {
        let pokemon: Pokemon?
        init(_ pokemon: Pokemon?) {
            self.pokemon = pokemon
        }
        func reduce(state: inout HomeState) {
            state.selection = pokemon
        }
    }
}
