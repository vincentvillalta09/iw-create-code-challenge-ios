//
//  HomeService.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import PromiseKit
import Alamofire

class HomeService {
    func requestPokemonList(_ limit: Int = 20, offset: Int = 0) -> Promise<PokemonAPIResponse> {
        return Promise { seal in
            let url = Server.baseUrl + Server.pokemonListEndpoit + "?offset=\(offset)&limit=\(limit)"
            AF.request(url).responseJSON { response in
                if let error = response.error {
                    seal.reject(error)
                }
                
                if let result = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let apiResponse = try decoder.decode(PokemonAPIResponse.self, from: result)
                        seal.fulfill(apiResponse)
                    } catch {
                        seal.reject(error)
                    }
                }
            }
        }
    }
    
    func requestPokemonDetails(_ pokemon: Pokemon) -> Promise<PokemonDetailsAPIResponse>{
        return Promise { seal in
            AF.request(pokemon.url).responseJSON { response in
                if let error = response.error {
                    seal.reject(error)
                }
                
                if let result = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let apiResponse = try decoder.decode(PokemonDetailsAPIResponse.self, from: result)
                        seal.fulfill(apiResponse)
                    } catch {
                        seal.reject(error)
                    }
                }
            }
        }
    }
    
    func requestPokemonImages(_ pokemonForm: Form) -> Promise<PokemonFormAPIResponse>{
        return Promise { seal in
            AF.request(pokemonForm.url).responseJSON { response in
                if let error = response.error {
                    seal.reject(error)
                }
                
                if let result = response.data {
                    do {
                        let decoder = JSONDecoder()
                        let apiResponse = try decoder.decode(PokemonFormAPIResponse.self, from: result)
                        print(apiResponse)
                        seal.fulfill(apiResponse)
                    } catch {
                        seal.reject(error)
                    }
                }
            }
        }
    }
}
