//
//  HomeViewModel.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

struct HomeViewModel {
    var isLoading: Bool
    var pokemonList: [Pokemon]
    var selectedPokemon: Pokemon?
}

extension HomeViewModel {
    init(_ state: HomeState) {
        self.isLoading = state.isLoading
        self.pokemonList = state.pokemons
        self.selectedPokemon = state.selection
    }
}
