//
//  PokemonListCell.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit
import Kingfisher

class PokemonListCell: UICollectionViewCell {
    private lazy var pokemonImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private lazy var pokemonNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.App.darkPurple
        return label
    }()
    
    private lazy var cellNumber: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.App.darkPurple
        return label
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [cellNumber, pokemonNameLabel])
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        return stackView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func loadUI() {
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        
        let nameWrapper = UIView()
        nameWrapper.backgroundColor = UIColor.white
        addSubview(nameWrapper)
        nameWrapper.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        nameWrapper.addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(8)
        }
        
        addSubview(pokemonImage)
        pokemonImage.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.bottom.equalTo(stackView.snp.top)
        }
    }
    
    func configureWith(_ pokemon: Pokemon, number: String) {
        self.pokemonNameLabel.text = pokemon.name
        self.cellNumber.text = number
        if let spriteURL = pokemon.sprites?["front_default"] {
            pokemonImage.kf.setImage(with: URL(string: spriteURL))
        }
    }
}
