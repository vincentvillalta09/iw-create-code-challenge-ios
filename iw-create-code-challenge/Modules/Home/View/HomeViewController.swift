//
//  HomeViewController.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit
import ReSwift
class HomeViewController: UIViewController {
    private let store: Store<AppState>
    private var state: HomeState?
    
    private lazy var homeView: HomeView = {
        let view = HomeView()
        view.delegate = self
        return view
    }()
    
    init(store: Store<AppState> = Dependency.shared.store) {
        self.store = store
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .default }
    
    override func loadView() {
        self.view = homeView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        store.dispatch(HomeAction.getPokemonList())
    }
}

extension HomeViewController: StoreSubscriber {
    typealias StoreSubscriberStateType = HomeState

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        store.subscribe(self) { state in
            return state.select({$0.homeState}).skipRepeats()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        store.unsubscribe(self)
    }
    
    func newState(state: StoreSubscriberStateType) {
        self.state = state
        let viewModel = HomeViewModel(state)
        homeView.updateView(with: viewModel)
    }
}

extension HomeViewController: HomeViewDelegate {
    func didSelectPokemon(_ pokemon: Pokemon) {
        store.dispatch(HomeAction.selectPokemon(pokemon))
        let viewController = PokemonDetailsViewController()
        self.present(viewController, animated: true, completion: nil)
    }
}
