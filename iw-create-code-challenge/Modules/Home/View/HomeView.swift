//
//  HomeView.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

protocol HomeViewDelegate: class {
    func didSelectPokemon(_ pokemon: Pokemon)
}

class HomeView: View {
    //////////////////////////////////
    // MARK: - Private properties
    //////////////////////////////////
    private var pokemons: [Pokemon] = []
    private lazy var collectionView: UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        layout.minimumLineSpacing = 20
        let cellSize = (UIScreen.main.bounds.width / 2) - 40
        layout.itemSize = CGSize(width: cellSize, height: cellSize)
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(PokemonListCell.self, forCellWithReuseIdentifier: "PokemonListCell")
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    
    //////////////////////////////////
    // MARK: - Public properties
    //////////////////////////////////
    weak var delegate: HomeViewDelegate?
    
    //////////////////////////////////
    // MARK: - Update View
    //////////////////////////////////
    public func updateView(with viewModel: HomeViewModel) {
        BlockingLoadingIndicator.show(on: self, when: viewModel.isLoading)
        self.pokemons = viewModel.pokemonList
        self.collectionView.reloadData()
    }
    
    override func commonInit() {
        super.commonInit()
        backgroundColor = UIColor.App.darkPurple
        
        addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

extension HomeView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokemonListCell", for: indexPath) as? PokemonListCell {
            cell.configureWith(self.pokemons[indexPath.row], number: "#\(indexPath.row + 1)")
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pokemons.count
    }
}

extension HomeView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didSelectPokemon(self.pokemons[indexPath.row])
    }
}
