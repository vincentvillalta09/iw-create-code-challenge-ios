//
//  UIColor+Extensions.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

extension UIColor {
    convenience init?(optionalHex: String?) {
        if let hex = optionalHex {
            self.init(hex: hex)
        } else {
            return nil
        }
    }
    convenience init(hex: String, alpha: CGFloat = 1.0) {
        let sanitizedHex = hex.replacingOccurrences(of: "#", with: "")
        let scanner = Scanner(string: sanitizedHex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff,
            alpha: alpha
        )
    }
    
    var brightnessValue: CGFloat {
        guard let components = cgColor.components, components.count > 2 else {return 0.0}
        return ((components[0] * 299) + (components[1] * 587) + (components[2] * 114)) / 1000
    }
    
    struct App {
        static let darkPurple = UIColor(hex: "#2A0D2E")
        static let pink = UIColor(hex: "#FA5075")
    }
    
}
