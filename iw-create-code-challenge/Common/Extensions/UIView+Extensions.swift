//
//  UIView+Extensions.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

extension UIView {
    /// Search the receiver's immediate subviews and return true if one is of the provided type.
    func hasSubviewOfType(_ subviewType: AnyClass) -> Bool {
        for subview in subviews where subview.isKind(of: subviewType) {
            return true
        }
        return false
    }
    
    /// Search the receiver's immediate subviews and return the subview (if present) that matches
    /// the provided type.
    func subviewOfType<T: AnyObject>(_ subviewType: T.Type) -> T? {
        for subview in subviews where subview.isKind(of: subviewType) {
            return subview as? T
        }
        return nil
    }
}
