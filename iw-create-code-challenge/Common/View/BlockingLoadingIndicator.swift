//
//  BlockingLoadingIndicator.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import UIKit

class BlockingLoadingIndicator: View {
    
    //////////////////////////////////
    // MARK: - Class Public Usage
    //////////////////////////////////

    static func show(on view: UIView, when condition: Bool) {
        if condition {
            show(on: view)
        } else {
            hide(on: view)
        }
    }
    
    static func show(on view: UIView) {
        guard !view.hasSubviewOfType(BlockingLoadingIndicator.self) else { return }
        
        let indicator = BlockingLoadingIndicator()
        indicator.activityIndicator.startAnimating()
        
        view.addSubview(indicator)
        indicator.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        indicator.alpha = 0.0
        UIView.animate(withDuration: 0.3) {
            indicator.alpha = 1.0
        }
    }
    
    static func hide(on view: UIView) {
        guard let indicator = view.subviewOfType(BlockingLoadingIndicator.self) else { return }
        UIView.animate(withDuration: 0.3, animations: {
            indicator.alpha = 0.0
        }, completion: { _ in
            indicator.removeFromSuperview()
        })
    }

    //////////////////////////////////
    // MARK: - Private
    //////////////////////////////////

    private lazy var activityIndicator: UIActivityIndicatorView = {
        return UIActivityIndicatorView(style: .white)
    }()
    
    //////////////////////////////////
    // MARK: - Overrides
    //////////////////////////////////

    override func commonInit() {
        super.commonInit()
        
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
        
        addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
