//
//  Constants.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation

struct Server {
    static let baseUrl = "https://pokeapi.co/api/v2/"
    // /pokemon?offset=20&limit=20
    static let pokemonListEndpoit = "pokemon"
}
