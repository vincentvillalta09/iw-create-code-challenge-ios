//
//  Dependency.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
import ReSwift
import ReSwiftThunk
class Dependency {
    static let shared = Dependency()
    private let thunkMiddleware: Middleware<AppState> = createThunkMiddleware()

    func makeMiddleware() -> [Middleware<AppState>] {
        return [
            thunkMiddleware
        ]
    }
    
    lazy var store: Store<AppState> = {
        return Store(reducer: appReducer,
                     state: AppState(),
                     middleware: makeMiddleware(),
                     automaticallySkipsRepeats: true)
    }()
}
