//
//  Pokemon.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation
struct Pokemon: Codable, Equatable {
    var name: String
    var url: String
    var abilities: [Abilities]?
    var forms: [Form]?
    var height: Int?
    var pokemonId: Int?
    var sprites: Dictionary<String, String>?
}

struct Abilities: Codable, Equatable {
    var ability: AbilityObject
    var slot: Int
}

struct AbilityObject: Codable, Equatable {
    var name: String
    var url: String
}

struct Form: Codable, Equatable {
    var name: String
    var url: String
}
