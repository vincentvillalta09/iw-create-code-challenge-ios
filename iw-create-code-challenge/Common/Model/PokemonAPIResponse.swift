//
//  PokemonAPIResponse.swift
//  iw-create-code-challenge
//
//  Created by Vincent Villalta on 10/19/20.
//

import Foundation

struct PokemonAPIResponse: Codable {
    var count: Int
    var next: String?
    var previous: String?
    var results: [Pokemon]
}

struct PokemonDetailsAPIResponse: Codable {
    var abilities: [Abilities]
    var forms: [Form]
    var height: Int
}

struct PokemonFormAPIResponse: Codable {
    var sprites: Dictionary<String, String>
}
